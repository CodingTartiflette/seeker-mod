package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.ai.SKR_akitaAI;
import data.scripts.ai.SKR_antiMissileAI;
import data.scripts.ai.SKR_flareAI;
import data.scripts.ai.SKR_modsAI;
import data.scripts.ai.SKR_obsidianMissileAI;
import data.scripts.ai.SKR_oversteerMissileAI;
import data.scripts.ai.SKR_pilumAI;
import data.scripts.ai.SKR_spikeAI;
import data.scripts.ai.SKR_stepMissileAI;
import data.scripts.ai.SKR_sunburstMissileAI;
import org.dark.shaders.light.LightData;
import org.dark.shaders.util.ShaderLib;
import org.dark.shaders.util.TextureData;

public class SKR_modPlugin extends BaseModPlugin {

    public static final String antiMissile_ID = "SKR_antiMissile";
    public static final String stepMissile_ID = "SKR_stepMissile";
    public static final String accelMissile_ID = "SKR_accelMissile";
    public static final String spike_ID = "SKR_spike_rod";
    public static final String flare_ID = "SKR_flareMirv_shot";
    public static final String sunburst_ID = "SKR_sunburstMissile";
    public static final String obsidian_ID = "SKR_obsidianMissile";
    public static final String mods_ID = "SKR_modsMissile";
    public static final String pilum_ID = "SKR_pilumMissile";
    public static final String stare_ID = "SKR_akitaMissile";
    

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        switch (missile.getProjectileSpecId()) {
            case antiMissile_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_antiMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case stepMissile_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_stepMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case accelMissile_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_oversteerMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case spike_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_spikeAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case flare_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_flareAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case sunburst_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_sunburstMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case obsidian_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_obsidianMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case mods_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_modsAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case pilum_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_pilumAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case stare_ID:
                return new PluginPick<MissileAIPlugin>(new SKR_akitaAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            default:
        }
        return null;
    }
    
//    @Override
//    public PluginPick<AutofireAIPlugin> pickWeaponAutofireAI(WeaponAPI weapon)
//    {
//        switch (weapon.getId())
//        {
//            case dimentionLeft_ID:
//                return new PluginPick<AutofireAIPlugin>(new ART_dimention_beamForceFire(weapon), CampaignPlugin.PickPriority.MOD_SET);
//            case dimentionRight_ID:
//                return new PluginPick<AutofireAIPlugin>(new ART_dimention_beamForceFire(weapon), CampaignPlugin.PickPriority.MOD_SET);
//            default:
//                return null;
//        }
//    }
    
    @Override
    public void onApplicationLoad() throws ClassNotFoundException {
        try {
            Global.getSettings().getScriptClassLoader().loadClass("org.lazywizard.lazylib.ModUtils");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "LazyLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download LazyLib at http://fractalsoftworks.com/forum/index.php?topic=5444"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }
        
        if(Global.getSettings().getModManager().isModEnabled("shaderLib")){
            ShaderLib.init();  
            LightData.readLightDataCSV("data/lights/SKR_light_data.csv"); 
            TextureData.readTextureDataCSV("data/lights/SKR_texture_data.csv"); 
        }
    }
}
