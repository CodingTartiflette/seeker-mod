package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.util.MagicUI;
import java.awt.Color;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;

//By Tartiflette

public class SKR_heatEffect implements EveryFrameWeaponEffectPlugin {
    
    private final float MAX_SHIELD=10;
    
    private boolean alive = true, runOnce = false, disabled=false;
    private int range = 0;
    private float shieldT=0,aiT=0;
    private ShipAPI ship;
    private ShieldAPI shield;       
    
    private IntervalUtil timer = new IntervalUtil(0.05f,0.15f);
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if (engine.isPaused() || !alive) {return;}
        
        if (!runOnce){
            runOnce=true;
            ship=weapon.getShip();
            shield=ship.getShield();
        }
        
        timer.advance(amount);
        if(timer.intervalElapsed()){
            
            float flux = ship.getFluxTracker().getFluxLevel();
            if (flux>0){
                weapon.getAnimation().setFrame(1);                
            } else {
                weapon.getAnimation().setFrame(0);
            }
            
            float red = Math.min(
                    1,
                    Math.max(
                            0,
                            (float)FastTrig.cos( MathUtils.FPI/2  * (1-flux))
                    )
            );
            
            weapon.getSprite().setColor(new Color(red,flux,flux,1));
            
            if (ship != null && !ship.isAlive()) {
                if (range>=1){
                    weapon.getAnimation().setFrame(0);
                    alive = false;
                }
                range++;
            }
        }
        
        if(shield!=null){
            if(!disabled){
                if(shield.isOn()){
                    shieldT=Math.min(MAX_SHIELD, shieldT+amount);
                } else {
                    shieldT=Math.max(0, shieldT-(amount*2));
                }
                
                //UI
                float color=shieldT/MAX_SHIELD;
//                MagicUI.drawSystemBar(ship,new Color(color,1,(1-color)/2), shieldT/MAX_SHIELD, 0);
                MagicUI.drawInterfaceStatusBar(ship, 1-shieldT/MAX_SHIELD, new Color(color*0.4f+0.6f,1f-0.4f*color,0), null, 0, "SHIELD", (int) (100-100*shieldT/MAX_SHIELD));
                       
                //AI
                if(ship.getAIFlags()!=null){
                    aiT+=amount;
                    if(aiT>0.5f){
                        aiT=0;

                        if(
                                !ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.DO_NOT_USE_SHIELDS) &&
                                Math.random()>0.75f &&
                                (shieldT/MAX_SHIELD)>0.8f                               
                                ){
                            ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.DO_NOT_USE_SHIELDS,2);
                        }
                    }
                }
                
                if(shieldT==MAX_SHIELD){
                    shield.toggleOff();
                    ship.getFluxTracker().showOverloadFloatyIfNeeded("Shield Offline", Color.red, 0, true);
                    ship.getFluxTracker().beginOverloadWithTotalBaseDuration(0.5f);
                    disabled=true;
                    //prevent AI use
                    if(ship.getAIFlags()!=null){
                        ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.DO_NOT_USE_SHIELDS,MAX_SHIELD*2.1f);
                    }
                }
                
            } else {
                shield.toggleOff();
                shieldT=Math.max(0, shieldT-(amount/2));
                
//                MagicUI.drawSystemBar(ship,new Color(255,0,0), shieldT/MAX_SHIELD,0);
                MagicUI.drawInterfaceStatusBar(ship, 1-shieldT/MAX_SHIELD, Color.RED, null, 0, "SHIELD", (int) (100-100*shieldT/MAX_SHIELD));
                
                if(shieldT==0){
                    disabled=false;
                    ship.getFluxTracker().showOverloadFloatyIfNeeded("Shield Online", Color.green, 0, true);
                }
            }
        }
    }
}