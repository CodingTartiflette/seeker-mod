//by Tartiflette,
//feel free to use it, credit is appreciated but not mandatory
package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
import data.scripts.util.SKR_interference;

public class SKR_baseInterferenceEffect implements EveryFrameWeaponEffectPlugin {
    
    private boolean runOnce=false;
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if(!runOnce){
            runOnce=true;            
            //only affect non built-in
            if(!weapon.getSlot().isBuiltIn()){                
                //INTERFERENCE CHECK
                SKR_interference.ApplyInterference(weapon.getShip(), weapon);
                //INTERFERENCE CHECK
            }
        }
    }
}