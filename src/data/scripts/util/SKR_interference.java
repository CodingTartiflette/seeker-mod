/*
By Tartiflette
 */
package data.scripts.util;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SKR_interference {    
    
    //////////////////////////////
    //                          //
    //   INTERFERENCE EFFECT    //
    //                          //
    //////////////////////////////  

    /**
     * Reduces a ship's passive dissipation if more than one weapon causing interferences is installed. The strength of the effect has a quadratic growth with the number of such weapons installed.
     * 
     * @param ship
     * ShipAPI affected by the interfering weapons
     * 
     * @param weapon
     * WeaponAPI source of the interference debuff
     */
    
    public static void ApplyInterference (ShipAPI ship, WeaponAPI weapon){
        //get interference data
        mergeInterference();
        
        //get the number of interference sources
        int interfere=0;
        for(WeaponAPI w : weapon.getShip().getAllWeapons()){
            if(WEAPONS.contains(w.getId())){
                interfere++;
            }
        }
        
        float mult=1;
        if(ship.getVariant().getHullMods().contains("fluxbreakers")){
            log.error("Resistant Flux Conduits installed, debuff halved.");
            mult=RFC;
        }
        
        //apply the effect if required
        if(interfere>1){
            
            //flux reduction
            weapon.getShip().getMutableStats().getFluxDissipation().modifyFlat(
                    weapon.toString(), 
                    -(interfere-1)*RATES.get(weapon.getSize())*mult
            );
            
            //if not present, add the warning hullmod
            if(weapon.getShip().getOriginalOwner()==-1){
                if(!weapon.getShip().getVariant().getHullMods().contains(WARNING)){
                    weapon.getShip().getVariant().getHullMods().add(WARNING);
                }
            }
        } else if(weapon.getShip().getOriginalOwner()==-1 && weapon.getShip().getVariant().getHullMods().contains(WARNING)){
            weapon.getShip().getVariant().getHullMods().remove(WARNING);
        }
    }
    
    //////////////////////////////
    //                          //
    //    INTERFERENCE DATA     //
    //                          //
    //////////////////////////////            
        
    private static Logger log = Global.getLogger(SKR_interference.class);
    private static final String WARNING="interferenceWarning";
    private static final String INTERFERENCE = "data/config/seeker/interference.csv";
    private static List<String>WEAPONS=new ArrayList<>();
    private static final String SETTINGS = "data/config/seeker/interference_settings.csv";
    private static Map<WeaponAPI.WeaponSize, Integer> RATES = new HashMap<>();
    private static float RFC;
    
    private static void mergeInterference(){
        //read interference.csv
        try {
            JSONArray weapons = Global.getSettings().getMergedSpreadsheetDataForMod("id", INTERFERENCE, "seeker");
            for(int i=0; i<weapons.length();i++){
                JSONObject row = weapons.getJSONObject(i);
                WEAPONS.add(row.getString("id"));
            }
        } catch (IOException | JSONException ex) {
            log.error("unable to read interference.csv");
        }  
        //read interference_settings.csv
        try {            
            JSONArray settings = Global.getSettings().getMergedSpreadsheetDataForMod("small", SETTINGS, "seeker");            
            JSONObject row = settings.getJSONObject(0);
            RATES.put(WeaponAPI.WeaponSize.SMALL, row.getInt("small"));
            RATES.put(WeaponAPI.WeaponSize.MEDIUM, row.getInt("medium"));
            RATES.put(WeaponAPI.WeaponSize.LARGE, row.getInt("large"));
            RFC= (float)row.getDouble("RFCmult");
            
        } catch (IOException | JSONException ex) {
            log.error("unable to read interference_settings.csv");
        }  
    }
}