package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.log4j.Logger;

public class InterferenceWarning extends BaseHullMod {    
    
    private static Logger log = Global.getLogger(InterferenceWarning.class);
    private static final String INTERFERENCE = "data/config/seeker/interference.csv";
    private static List<String>WEAPONS=new ArrayList<>();
    private static final String SETTINGS = "data/config/seeker/interference_settings.csv";
    private static Map<WeaponAPI.WeaponSize, Integer> RATES = new HashMap<>();    
    private static float RFC;
    
    private void mergeInterference(){
        //read interference.csv
        try {
            log.error("loading interference.csv");
            JSONArray weapons = Global.getSettings().getMergedSpreadsheetDataForMod("id", INTERFERENCE, "seeker");
            for(int i=0; i<weapons.length();i++){
                JSONObject row = weapons.getJSONObject(i);
                WEAPONS.add(row.getString("id"));                
                log.error("interference source: "+row.getString("id"));
            }
        } catch (IOException | JSONException ex) {
            log.error("unable to read interference.csv");
        }
        //read interference_settings.csv
        try {
            log.error("loading interference_settings.csv");
            JSONArray settings = Global.getSettings().getMergedSpreadsheetDataForMod("small", SETTINGS, "seeker");            
            JSONObject row = settings.getJSONObject(0);
            RATES.put(WeaponAPI.WeaponSize.SMALL, row.getInt("small"));
            log.error("small weapon interference rate: "+row.getInt("small"));
            RATES.put(WeaponAPI.WeaponSize.MEDIUM, row.getInt("medium"));
            log.error("medium weapon interference rate: "+row.getInt("medium"));
            RATES.put(WeaponAPI.WeaponSize.LARGE, row.getInt("large"));
            log.error("large weapon interference rate: "+row.getInt("large"));
            RFC= (float)row.getDouble("RFCmult");
            log.error("RFC hullmod effect reduction : "+RFC);
        } catch (IOException | JSONException ex) {
            log.error("unable to read interference_settings.csv");
        }
    }
    
    private Integer computeDebuff (ShipAPI ship){
        
        log.error("computing interference debuff");
        List<WeaponAPI> sources = new ArrayList<>();
        //scan all weapons for interference sources
        for(WeaponAPI w : ship.getAllWeapons()){
            if(WEAPONS.contains(w.getId())){
                sources.add(w);
                log.error("added interference source: "+w.getId());
            }
        }
        //compute all the debuff
        int debuff=0;
        log.error("found "+sources.size()+" interference sources");
        if(sources.size()>1){
            for (WeaponAPI w : sources){
                debuff+=(sources.size()-1)*RATES.get(w.getSize());
            }
        }
        if(ship.getVariant().getHullMods().contains("fluxbreakers")){
            debuff*=RFC;
            log.error("Resistant Flux Conduits installed, debuff reduced.");
        }
        log.error("total debuff: "+debuff);
        return debuff;
    }
    
    private Map<ShipAPI, Integer> INTERFERED = new HashMap<>();
    
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        if(WEAPONS.isEmpty()){
            mergeInterference();
        }
        if(!INTERFERED.containsKey(ship)){
            INTERFERED.put(ship, computeDebuff(ship));
        }
        if(Global.getCombatEngine().getPlayerShip()==ship){
            Global.getCombatEngine().maintainStatusForPlayerShip(
                    "Interference",
                    "graphics/SEEKER/icons/hullsys/SKR_interference.png",
                    "Weapons Interference:",                        
                    "-"+INTERFERED.get(ship)+" Flux dissipation",
                    true
            );
        }
    }
    
    @Override
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        mergeInterference();
        if (index == 0) return "WARNING";        
        if (index == 1) return RATES.get(WeaponSize.SMALL)+"/"+RATES.get(WeaponSize.MEDIUM)+"/"+RATES.get(WeaponSize.LARGE);
        if (index == 2) return Math.round(100*(1-RFC))+"%";
        if (index == 3) return "Resistant Flux Conduits";
        return null;
    }
}
