package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import data.scripts.util.MagicAnim;
//import org.lwjgl.util.vector.Vector2f;

public class SKR_alcubiereStats extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {

//        stats.getAcceleration().modifyMult(id, 0);
//        stats.getDeceleration().modifyMult(id, 0);
        stats.getTurnAcceleration().modifyMult(id, 20f);
        stats.getMaxTurnRate().modifyFlat(id, MagicAnim.RSO(effectLevel, 0, 1)*180f);
    }
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getMaxTurnRate().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
//        stats.getAcceleration().unmodify(id);
//        stats.getDeceleration().unmodify(id);
        
        //clamp mobility
        float turning = stats.getMaxTurnRate().getBaseValue();
        stats.getEntity().setAngularVelocity(Math.min(turning, Math.max(-turning, stats.getEntity().getAngularVelocity())));
//        Vector2f velocity = stats.getEntity().getVelocity();
//        velocity.set(0,0);
    }
	
    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("translation in progress", false);
        } else if (index == 1) {
            return new StatusData("repulsion field active", false);
        }
        return null;
    }
}
