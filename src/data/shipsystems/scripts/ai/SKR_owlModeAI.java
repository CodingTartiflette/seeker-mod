package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class SKR_owlModeAI implements ShipSystemAIScript
{
    private CombatEngineAPI engine;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private float nominalRange=0;
    private boolean runOnce = false;
    private boolean active = false;
    private final IntervalUtil checkAgain = new IntervalUtil (1f,2f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine)
    {
        this.ship = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target)
    {        
        if (engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }

        if (engine.isPaused() || ship.getShipAI()==null) {
            return;
        }        

        if(!runOnce){
            runOnce=true;
            //calculate the nominal range: the average range of all medium weapons
            List<WeaponAPI> weapons=ship.getAllWeapons();
            int i = 0;
            for (WeaponAPI w : weapons) {
                 if ( w.getSize() == WeaponAPI.WeaponSize.MEDIUM ) {
                    nominalRange = nominalRange + w.getRange();
                    i++;
                }        
            }
            nominalRange = nominalRange/i;
        }
        
        //prevent the system activation if retreating
        if (ship.isRetreating()){
            if(system.isActive()){ship.useSystem();}
            return;
        }
        
        checkAgain.advance(amount);
        
        if (checkAgain.intervalElapsed() && target!=null) {
            //check if the target is in weapons range            
            if ( MathUtils.getDistance(ship, target) < 0.8f*nominalRange ){
                //if the target is close enough
                if (ship.getDeployedDrones().isEmpty() && AIUtils.canUseSystemThisFrame(ship)){
                    //if the system is inactive, activate
                    active = true;
                    ship.useSystem();                    
                }
            } else if((ship.getFluxTracker().getFluxLevel()>0.85f || MathUtils.getDistance(ship, target) > 1.1f*nominalRange) && !ship.getDeployedDrones().isEmpty() && active){
                active = false;
                ship.useSystem();
            } 
        }
    }
}
