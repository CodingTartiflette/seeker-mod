package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class SKR_owlModeStats extends BaseShipSystemScript {
    
    @Override
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        stats.getMaxSpeed().modifyMult(id, 0.5f);
//        stats.getAcceleration().modifyMult(id, 5);
//        stats.getTurnAcceleration().modifyMult(id, 5);
//        stats.getMaxTurnRate().modifyMult(id, 5);
//        stats.getFluxDissipation().modifyMult(id, 1.15f);
//        stats.getBallisticRoFMult().modifyMult(id, 1.15f);
//        stats.getBallisticWeaponRangeBonus().modifyMult(id, 1.15f);
        stats.getShieldAbsorptionMult().modifyMult(id, 0.85f);
        stats.getShieldUpkeepMult().modifyMult(id, 0.85f);
        
        //using percentage modifier for proper additive boost
        stats.getAcceleration().modifyPercent(id, 400);
        stats.getTurnAcceleration().modifyPercent(id, 400);
        stats.getMaxTurnRate().modifyPercent(id, 400);
        stats.getFluxDissipation().modifyPercent(id, 15f);
        stats.getBallisticRoFMult().modifyPercent(id, 15f);
        stats.getBallisticWeaponRangeBonus().modifyPercent(id, 15f);
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getMaxSpeed().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getFluxDissipation().unmodify(id);
        stats.getBallisticRoFMult().unmodify(id);
        stats.getBallisticWeaponRangeBonus().unmodify(id);
        stats.getShieldAbsorptionMult().unmodify(id);
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        if (index == 0) {
            return new ShipSystemStatsScript.StatusData("-50% max speed, +20% Combat performances.", false);
        }
        return null;
    }
}
