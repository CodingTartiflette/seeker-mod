package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;

public class SKR_twinShieldStats extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        stats.getShieldDamageTakenMult().modifyMult(id, 1-(effectLevel/2));
//        stats.getShieldTurnRateMult().modifyMult(id, 4*effectLevel);
        stats.getShieldTurnRateMult().modifyPercent(id, 300*effectLevel);
    }
    
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getShieldDamageTakenMult().unmodify(id);
        stats.getShieldTurnRateMult().unmodify(id);
    }
    
    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("+"+(int)Math.round(50*effectLevel)+"% shield damage reduction", false);
        }
        if (index == 1) {
            return new StatusData("-"+(int)Math.round(50*effectLevel)+"% shield arc", false);
        }
        return null;
    }
}